"use strict";
//
// function createNewUser() {
//     let firstName = prompt("Input your Firstname:");
//     let lastName = prompt("Input your Lastname:");
//
//     while (firstName === "" || firstName == null || lastName === "" || lastName == null){
//         alert("You have not entered Firstname or Lastname!!");
//         firstName = prompt("Input correct Firstname:");
//         lastName = prompt("Input correct Lastname:");
//     }
//
//     let newUser = {
//         firstName,
//         lastName,
//
//         getLogin() {
//            return (this.firstName[0] + this.lastName).toLowerCase();
//         }
//      }
//     return newUser;
// }


function firstName() {
    let firstName = prompt("Input your Firstname:");
    while (firstName === "" || firstName == null){
        alert("You have not entered Firstname or Lastname!!");
        firstName = prompt("Input correct Firstname:");
    }
    return firstName;
}

function lastName() {

    let lastName = prompt("Input your Lastname:");

    while (lastName === "" || lastName == null){
        alert("You have not entered Firstname or Lastname!!");
        lastName = prompt("Input correct Lastname:");
    }
    return lastName;
}

function createNewUser() {

    const newUser = {
        firstName: firstName(),
        lastName: lastName(),

        get getFirstName() {
            return this.firstName;
        },

        set setFirstName(newFirstName) {
            this.firstName = newFirstName;
        },

        get getLastName() {
            return this.lastName;
        },

        set setLastName(newLastName) {
            this.lastName = newLastName;
        },

        getLogin() {
            return (this.firstName[0].toLowerCase() + this.lastName.toLowerCase());
        }
    }
    return newUser;
}

console.log(createNewUser().getLogin());